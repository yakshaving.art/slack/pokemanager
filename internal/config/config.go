package config

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Config contains the configuration
type Config struct {
	Escalations        map[string][]Escalation `yaml:"escalations"`
	PrivateMessageDeny string
}

// Escalation contains the detail about an escalation
type Escalation struct {
	Targets              []string
	EscalateAfterSeconds int32
}

// Load loads the configuration
func Load(configFile string) (Config, error) {
	b, err := ioutil.ReadFile(configFile)
	if err != nil {
		return Config{}, fmt.Errorf("error reading file %s: %v", configFile, err)
	}

	config, err := ParseYAML(b)
	if err != nil {
		return Config{}, fmt.Errorf("error parsing YAML for file %s: %v", configFile, err)
	}

	return config, nil
}

func ParseYAML(input []byte) (Config, error) {
	var c Config
	err := yaml.Unmarshal(input, &c)
	if err != nil {
		return Config{}, fmt.Errorf("failed to parse config file: %w", err)
	}
	return c, nil
}
