package config

import (
	"flag"
	"os"
)

// Args are the arguments that can be used in this binary.
type Args struct {
	ConfigFile string
	Debug      bool
	SlackToken string
	Version    bool
}

// ParseArgs parses the command line arguments
func ParseArgs() Args {
	args := Args{}

	flag.BoolVar(&args.Debug, "debug", false, "enable debug level logging")
	flag.BoolVar(&args.Version, "version", false, "show version and exit")
	flag.StringVar(&args.ConfigFile, "config", "pokemanager.yml", "path to the configuration file")

	flag.Parse()
	args.SlackToken = os.Getenv("SLACK_TOKEN")
	return args
}
