package slack

var myID = "U12345ABC"
var messages = []struct {
	msg         string
	shouldMatch bool
}{
	{`This message shouldn't match`, false},
	{`But this one should, <@U12345ABC>`, true},
	{`<@654321> is someone else`, false},
	{`<@654321> and <@U12345ABC> are good friends`, true},
}

// func TestMessageMatch(t *testing.T) {
// 	myHandle := fmt.Sprintf("<@%s>", myID)
// 	for _, tc := range messages {
// 		t.Run(tc.msg, func(t *testing.T) {
// 			result := slack.MatchMessage(tc.msg, myHandle)
// 			if result != tc.shouldMatch {
// 				t.Errorf("message %s: got %v, want %v", tc.msg, result, tc.shouldMatch)
// 			}
// 		})
// 	}
// }
