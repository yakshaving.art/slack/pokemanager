package slack

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
)

// Poke contains all that's needed to poke someone
// type Poke struct {
// 	AuthorID       string
// 	Channel        string
// 	Timeout        chan bool
// 	Message        string
// 	TimeoutSeconds int32
// }

// IncomingMessage contains an incoming message to pass on to pokemanager
type IncomingMessage struct {
	Action           string
	AuthorID         string
	Channel          string
	IsPrivateMessage bool
	PokeID           string
	Text             string
}

// Client contains all that is needed to talk to Slack and the channel
// to interact with pokemanager
type Client struct {
	APIClient        *slack.Client
	IncomingMessages chan IncomingMessage
	MessageParser    MessageParser
	RTM              *slack.RTM
}

// Connect establishes a connection with Slack
func Connect(token string, debug bool) *Client {
	// api := slack.New(token, slack.OptionDebug(debug))  // TODO: Restore this line
	api := slack.New(token)
	rtm := api.NewRTM(slack.RTMOptionUseStart(false))
	go rtm.ManageConnection()
	ch := make(chan IncomingMessage)
	return &Client{
		APIClient:        api,
		IncomingMessages: ch,
		MessageParser:    newMessageParser(rtm),
		RTM:              rtm,
	}
}

// Listen parses all incoming messages, matches those with mentions and sends
// them to pokemanager to be escalated
func (s Client) Listen() {
	defer close(s.IncomingMessages)
	for msg := range s.RTM.IncomingEvents {
		switch event := msg.Data.(type) {
		case *slack.MessageEvent:
			// // We need to wait for RTM to be up before fetching our own user ID
			// s.MessageParser.MyID = s.MessageParser.RTM.GetInfo().User.ID
			// if s.MessageParser.isMyOwnMessage(event) {
			// 	// TODO: Count me
			// 	continue
			// }
			if event.Type == "message" {
				s.IncomingMessages <- IncomingMessage{
					AuthorID:         event.User,
					Channel:          event.Channel,
					IsPrivateMessage: false,
					Text:             event.Text,
				}
			}

		case *slack.RTMError:
			// Count the error

		case *slack.InvalidAuthEvent:
			logrus.Fatal("Invalid credentials")

		default:
			// Do nothing
		}
	}
}

// PokeUser sends a private message to a user and expects an acknowledgement
// func (s Client) PokeUser(userID string, pokeID string, poke Poke) error {
// 	message := fmt.Sprintf("Incoming mention in %s:\n\n%s\n\n"+
// 		"Reply \"ack %s\" to take it, \"pass %s\" to ignore it.\n"+
// 		"You have %d seconds before I escalate it.",
// 		poke.Channel, poke.Message, pokeID, pokeID, poke.TimeoutSeconds)
// 	if err := s.sendMessage(userID, message); err != nil {
// 		return err
// 	}
// 	// TODO: Begin the countdown
// 	// TODO: Check that the user can acknowledge this poke
// 	return nil
// }

// InvalidReply sends a private message to remind a user that only
// the proposed options are accepted replies
func (s Client) InvalidReply(userID string) error {
	message := "Please reply \"ack\" or \"pass\""
	if err := s.sendMessage(userID, message); err != nil {
		return err
	}
	return nil
}

func (s Client) sendMessage(userID string, message string) error {
	if _, _, err := s.RTM.PostMessage(userID,
		slack.MsgOptionAsUser(true),
		slack.MsgOptionText(message, false),
	); err != nil {
		return fmt.Errorf("failed to post message: %s", err)
	}
	return nil
}
