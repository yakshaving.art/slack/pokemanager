package metrics

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/yakshaving.art/slack/pokemanager/internal/version"
)

var (
	namespace = "pokemanager"

	BootTime = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "boot_time_seconds",
		Help:      "unix timestamp of when the service was started",
	})

	BuildInfo = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "build_info",
		Help:      "build information",
	}, []string{"version", "commit", "date"})
)

// Exported metrics
var (
	ActiveURLs = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Subsystem: "urls",
		Name:      "active_total",
		Help:      "total number of URLs stored in memory",
	})
)

func init() {
	BootTime.SetToCurrentTime()
	BuildInfo.WithLabelValues(version.Version, version.Commit, version.Date).Set(1)
}

// Handler exposes the Prometheus metrics
func Handler() http.Handler {
	return promhttp.Handler()
}
