# Pokemanager

A Slack bot that routes incoming mentions following an escalation policy.

## Usage

As a Slack bot, pokemanager assumes a user identity. When mentioned in a
channel, it checks its configuration and sends a private message to the first
point of escalation, asking for acknowledgement. If ignored, it will escalate to
the following level until the policy ends. Then it will drop the request.

Pokemanager only accepts commands over Slack channels. That is, you cannot start
a private conversation and expect your request to be escalated. The operator can
configure a default message to reply to private messages and explain this
behaviour.

## Caveats

- Pokemanager won't join a channel and must be invited.

## Arguments

`--config-file` - The configuration file to load. See [below](#configuration)
for an example.

`--version` - Print the version and exists.

## Required Environment Variables

`SLACK_TOKEN` - The token used to connect to the Slack API.

## Configuration

`channels` (optional) is a list of channels where to listen for mentions. By
default, pokemanager listens on any channel.

`escalation_policy` (required) is a list in which each item represents an
escalation action. Items are processed sequentially. After the last action
pokemanager won't restart from the first one and will drop the request. Read
[below](#escalation_levels) about how to specity rules for each escalation
level.

`private_message_deny` (optional) is the message to use when replying to an
incoming private message.

### Escalation levels

`handlers` (required) lists the users to forward the message to.

`escalate_after_seconds` (optional) specifies how long to wait before escalating
to the next level. Defaults to 60. It can be omitted for the last escalation
level as pokemanager will drop the request after notifying its handlers.

### Example

```yaml
---
escalation_policy:
  -
    handlers:
    - dilbert
    escalate_after_seconds: 300
  -
    handlers:
    - asok
    - alice
  -
    handlers:
    - wally
    escalate_after_seconds: 120
  -
    handlers:
    - pointy_haired_boss
```

## FAQ

### Why aren't you using the Slack events API?

For simplicity sake. Although the code implementation would result more elegant,
setting up a Slack events API endpoint can be bothersome from the infrastructure
perspective: expose a public endpoint, TLS certificates, manually set up the
webhook url... All of this to receive only the mention events. We built the same
by parsing messages over RTM: less elegant but requires zero infrastructure
setup.
